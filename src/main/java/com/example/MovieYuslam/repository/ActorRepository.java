package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.MovieYuslam.model.Actor;

public interface ActorRepository extends JpaRepository<Actor, Long> {

	
}
