package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
		
}
