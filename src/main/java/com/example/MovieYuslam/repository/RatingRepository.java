package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Long> {

}
