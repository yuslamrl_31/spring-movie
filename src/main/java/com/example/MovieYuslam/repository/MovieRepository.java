package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

}
