package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.MovieGenres;

public interface MovieGenresRepository extends JpaRepository<MovieGenres, Long> {

}
