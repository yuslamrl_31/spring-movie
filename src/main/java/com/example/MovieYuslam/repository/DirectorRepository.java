package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.MovieYuslam.model.Director;

public interface DirectorRepository extends JpaRepository<Director, Long> {
	
	
	
}
