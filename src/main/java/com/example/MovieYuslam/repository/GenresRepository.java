package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.Genres;

public interface GenresRepository extends JpaRepository<Genres, Long>{

}
