package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.MovieCast;

public interface MovieCastRepository extends JpaRepository<MovieCast, Long> {

}
