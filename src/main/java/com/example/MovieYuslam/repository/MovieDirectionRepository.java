package com.example.MovieYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieYuslam.model.MovieDirection;

public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Long> {
		
}
