package com.example.MovieYuslam.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_direction")
public class MovieDirection {

	@EmbeddedId
	private MovieDirectionId id = new MovieDirectionId(); 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("dir_id")
	@JoinColumn(name="dir_id")
	private Director director;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("mov_id")
	@JoinColumn(name = "mov_id")
	private Movie movie;
	
	public MovieDirection() {
		
	}

	public MovieDirection(Director director, Movie movie) {
		super();
		this.director = director;
		this.movie = movie;
	}
	
	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	
}
