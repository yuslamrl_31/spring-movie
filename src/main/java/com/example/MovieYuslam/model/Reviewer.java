package com.example.MovieYuslam.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reviewer")

public class Reviewer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private long id;
	
	private String name;

	@OneToMany(fetch = FetchType.LAZY,  cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "reviewer" )
	private Set<Rating> ratings;
	
	public Reviewer() {
		
	}
	
	
	public Reviewer(long id, String name) {
		super();
		this.id = id;
		this.name = name;
		
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
