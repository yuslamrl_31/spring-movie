package com.example.MovieYuslam.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_cast")
public class MovieCast {
	
	@EmbeddedId
	private MovieCastId id =  new MovieCastId();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("act_id")
	@JoinColumn(name = "act_id")
	private Actor actor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("mov_id")
	@JoinColumn(name ="mov_id")
	private Movie movie;
	
	private String role;

	
	public MovieCast() {
		
	}
		
	public MovieCast(Actor actor, Movie movie, String role) {
		super();
		
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}


	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
	
}
