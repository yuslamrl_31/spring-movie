package com.example.MovieYuslam.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable

public class RatingId implements Serializable {

		@Column(name = "rev_id")
		private long revId;
		
		@Column(name =  "mov_id")
		private long movId;
		
		public RatingId () {
			
		}

		public RatingId(long revId, long movId) {
			super();
			this.revId = revId;
			this.movId = movId;
		}
		
		@Override
		public boolean equals(Object o) {
			
			if(this == o) return true;
			
			if (o == null || getClass() != o.getClass())
				return false;
			
			RatingId that = (RatingId) o;
			return Objects.equals(revId, that.revId) &&
					Objects.equals(movId, that.movId);
		}

		@Override
		public int hashCode() {
		
			return Objects.hash(revId, movId);
		}
}
