package com.example.MovieYuslam.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable

public class MovieCastId implements Serializable {

	
	@Column(name = "act_id")
	private long actId;
	
	@Column(name = "mov_id")
	private long movId;
	
	public MovieCastId() {
		
	}

	public MovieCastId(long actId, long movId) {
		super();
		this.actId = actId;
		this.movId = movId;
	}

	@Override
	public boolean equals(Object o) {
		
		if(this == o) return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		MovieCastId that = (MovieCastId) o;
		return Objects.equals(actId, that.actId) &&
				Objects.equals(movId, that.movId);
	}

	@Override
	public int hashCode() {
	
		return Objects.hash(actId, movId);
	}
	
	
	
	
}
