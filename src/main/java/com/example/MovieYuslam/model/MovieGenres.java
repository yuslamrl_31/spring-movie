package com.example.MovieYuslam.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="movie_genres")
public class MovieGenres {

	@EmbeddedId
	private MovieGenresId id = new MovieGenresId();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("mov_id")
	@JoinColumn(name = "mov_id")
	private Movie movie;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("gen_id")
	@JoinColumn(name="gen_id")
	private Genres genres;

	public MovieGenres() {
		
	}
	
	public MovieGenres(Movie movie, Genres genres) {
		super();
		this.movie = movie;
		this.genres = genres;
	}


	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Genres getGenres() {
		return genres;
	}

	public void setGenres(Genres genres) {
		this.genres = genres;
	}
	
	
	
}
