package com.example.MovieYuslam.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class Rating {
	
	@EmbeddedId
	private RatingId id = new RatingId();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("rev_id")
	@JoinColumn(name = "rev_id")
	private Reviewer reviewer;
	
	@ManyToOne(fetch  = FetchType.LAZY)
	@MapsId("mov_id")
	@JoinColumn(name = "mov_id")
	private Movie movie;
	
	private int revStars;
	
	private int numORatings;
	
	public Rating() {
		
	}

	

	public Rating(Reviewer reviewer, Movie movie, int revStars, int numORatings) {
		super();
		this.reviewer = reviewer;
		this.movie = movie;
		this.revStars = revStars;
		this.numORatings = numORatings;
	}



	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getRevStars() {
		return revStars;
	}

	public void setRevStars(int revStars) {
		this.revStars = revStars;
	}

	public int getNumORatings() {
		return numORatings;
	}

	public void setNumORatings(int numORatings) {
		this.numORatings = numORatings;
	}
	
	
	
	
	
}
