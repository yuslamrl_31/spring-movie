package com.example.MovieYuslam.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieDirectionId implements Serializable{

	@Column(name = "dir_id")
	private long dirId;
	
	@Column(name = "mov_id")
	private long movId;
		
	public long getDirId() {
		return dirId;
	}

	public void setDirId(long dirId) {
		this.dirId = dirId;
	}

	public long getMovId() {
		return movId;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	public MovieDirectionId() {
		
	}

	public MovieDirectionId(long dirId, long movId) {
		super();
		this.dirId = dirId;
		this.movId = movId;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(this == o) return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		MovieDirectionId that = (MovieDirectionId) o;
		return Objects.equals(dirId, that.dirId) &&
				Objects.equals(movId, that.movId);
	}
	
	@Override
	public int hashCode() {
	
		return Objects.hash(dirId, movId);
	}
	
}
