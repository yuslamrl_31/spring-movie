package com.example.MovieYuslam.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieGenresId implements Serializable {
		
	@Column(name = "mov_id")
	private long movId;
	
	@Column(name = "gen_id")
	private long genId;

	public long getMovId() {
		return movId;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	public long getGenId() {
		return genId;
	}

	public void setGenId(long genId) {
		this.genId = genId;
	}
	
	public MovieGenresId() {
		
	}

	public MovieGenresId(long movId, long genId) {
		super();
		this.movId = movId;
		this.genId = genId;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(this == o) return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		MovieGenresId that = (MovieGenresId) o;
		return Objects.equals(movId, that.movId) &&
				Objects.equals(genId, that.genId);
	}

	@Override
	public int hashCode() {
	
		return Objects.hash(movId, genId);
	}
	
	
	
}
