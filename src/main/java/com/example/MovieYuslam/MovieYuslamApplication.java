package com.example.MovieYuslam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieYuslamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieYuslamApplication.class, args);
	}

}
