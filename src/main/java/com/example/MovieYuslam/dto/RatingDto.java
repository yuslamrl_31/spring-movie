package com.example.MovieYuslam.dto;

import com.example.MovieYuslam.model.Movie;
import com.example.MovieYuslam.model.Reviewer;

public class RatingDto {

	private Reviewer reviewer;
	
	private Movie movie;
	
	private int revStars;
	
	private int numORatings;

	public RatingDto() {
		
	}
	
	
	
	public RatingDto(Reviewer reviewer, Movie movie, int revStars, int numORatings) {
		super();
		this.reviewer = reviewer;
		this.movie = movie;
		this.revStars = revStars;
		this.numORatings = numORatings;
	}



	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getRevStars() {
		return revStars;
	}

	public void setRevStars(int revStars) {
		this.revStars = revStars;
	}

	public int getNumORatings() {
		return numORatings;
	}

	public void setNumORatings(int numORatings) {
		this.numORatings = numORatings;
	}
	
	
	
}
