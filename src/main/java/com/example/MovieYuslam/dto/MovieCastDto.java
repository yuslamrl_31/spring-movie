package com.example.MovieYuslam.dto;

import com.example.MovieYuslam.model.Actor;
import com.example.MovieYuslam.model.Movie;

public class MovieCastDto {
		
	private Actor actor;
	
	private Movie movie;
	
	private String role;

	public MovieCastDto() {
		
	}
	
	public MovieCastDto(Actor actor, Movie movie, String role) {
		super();
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}



	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
