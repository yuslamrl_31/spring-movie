package com.example.MovieYuslam.dto;

import com.example.MovieYuslam.model.Director;
import com.example.MovieYuslam.model.Movie;

public class MovieDirectionDto {

	private Director director;
	
	private Movie movie;

	public MovieDirectionDto() {
		
	}
	
	public MovieDirectionDto(Director director, Movie movie) {
		super();
		this.director = director;
		this.movie = movie;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	
	
	
	
}
