package com.example.MovieYuslam.dto;

import com.example.MovieYuslam.model.Genres;
import com.example.MovieYuslam.model.Movie;

public class MovieGenreDto {
		
	private Genres genres;
	
	private Movie movie;

	public MovieGenreDto() {
		
	}

	public MovieGenreDto(Genres genres, Movie movie) {
		super();
		this.genres = genres;
		this.movie = movie;
	}

	public Genres getGenres() {
		return genres;
	}

	public void setGenres(Genres genres) {
		this.genres = genres;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	
	
	
}
