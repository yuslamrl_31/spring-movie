package com.example.MovieYuslam.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MovieDto {

	private long id;
	
	private String title;
	
	private int year;
	
	private int time;
	
	private String lang;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	
	private String releaseCountry;

	public MovieDto() {
		
	}
	
	public MovieDto(long id, String title, int year, int time, String lang, Date releaseDate, String releaseCountry) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.time = time;
		this.lang = lang;
		this.releaseDate = releaseDate;
		this.releaseCountry = releaseCountry;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getReleaseCountry() {
		return releaseCountry;
	}

	public void setReleaseCountry(String releaseCountry) {
		this.releaseCountry = releaseCountry;
	}
	
	
	
}
