package com.example.MovieYuslam.dto;

public class GenresDto {

	private long id;
	
	private String title;

	public GenresDto() {
		
	}
	
	public GenresDto(long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
