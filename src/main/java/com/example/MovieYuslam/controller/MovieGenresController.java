package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.MovieGenreDto;
import com.example.MovieYuslam.model.MovieGenres;
import com.example.MovieYuslam.repository.MovieGenresRepository;

@RestController
@RequestMapping("/api")
	public class MovieGenresController {

		@Autowired
		MovieGenresRepository movieGenresRepository;
	
		//Get All Movie Genres
		@GetMapping("/read/movie_genres")
		public HashMap<String, Object> readMovieGenres(){
			HashMap<String, Object> hmMovieGenres = new HashMap<String, Object>();
			List<MovieGenreDto> genreDtos = new ArrayList<MovieGenreDto>();
			for(MovieGenres genres : movieGenresRepository.findAll()) {
				MovieGenreDto genreDto = new MovieGenreDto(genres.getGenres(), genres.getMovie());
				genreDtos.add(genreDto);
			}
				
			hmMovieGenres.put("Message : ", "Read All Movie Genres Success");
			hmMovieGenres.put("Total : ", genreDtos.size());			
			hmMovieGenres.put("Data : " , genreDtos);
				
			return hmMovieGenres;
			
			}
		
		//Create Movie Genres
		@PostMapping("/create/movie_genres")
		public HashMap<String, Object> createMovieGenres(@Valid @RequestBody MovieGenreDto movieGenreDtoDetails){
			HashMap<String, Object> hmMovieGenres = new HashMap<String, Object>();
			MovieGenres genres = new MovieGenres(movieGenreDtoDetails.getMovie(), movieGenreDtoDetails.getGenres());
					
			hmMovieGenres.put("Message :", "Create Movie Genres Succes");
			hmMovieGenres.put("Data : ", movieGenresRepository.save(genres));
				
			return hmMovieGenres;
			
			}
		
		//Delete Movie Genres
		@DeleteMapping("/delete/movie_genres/{id}")
		public HashMap<String, Object> deleteMovieGenres(@PathVariable(value = "id") Long id ){
			HashMap<String, Object> hmMovieGenres = new HashMap<String, Object>();
			MovieGenres genres = movieGenresRepository.findById(id).orElseThrow(null);
				
			movieGenresRepository.delete(genres);
				
			hmMovieGenres.put("Message : ", "Delete Movie Genres Succes");
			hmMovieGenres.put("Data :", genres);
				
			return hmMovieGenres;
			
			}
		
		//Update Movie Genres
		@PutMapping("/update/movie_genres/{id}")
		public HashMap<String, Object> updateMovieGenres(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieGenreDto movieGenreDtoDetails){
			HashMap<String, Object> hmMovieGenres = new HashMap<String, Object>();
			MovieGenres genres = movieGenresRepository.findById(id).orElseThrow(null);
				
				
				if(movieGenreDtoDetails.getMovie() != null) {
					genres.setMovie(movieGenreDtoDetails.getMovie());
				}
				
				if(movieGenreDtoDetails.getGenres() != null) {
					genres.setGenres(movieGenreDtoDetails.getGenres());
				}

				movieGenresRepository.save(genres);
					
				hmMovieGenres.put("Message : ", "Updated Movie Genres Succes");
				hmMovieGenres.put("Data : ", genres);
				
				return hmMovieGenres;
				}
}
