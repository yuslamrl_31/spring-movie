package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.MovieCastDto;
import com.example.MovieYuslam.model.MovieCast;
import com.example.MovieYuslam.repository.MovieCastRepository;


@RestController
@RequestMapping("/api")
public class MovieCastController {

	@Autowired
	MovieCastRepository movieCastRepository;
	
	//Get All Movie Cast
	@GetMapping("/read/movie_cast")
	public HashMap<String, Object> readMovieCast(){
		HashMap<String, Object> hmMovieCast = new HashMap<String, Object>();
		List<MovieCastDto> castDtos = new ArrayList<MovieCastDto>();
		for(MovieCast cast : movieCastRepository.findAll()) {
			MovieCastDto castDto = new MovieCastDto(cast.getActor(), cast.getMovie(), cast.getRole());
			castDtos.add(castDto);
		}
		
		hmMovieCast.put("Message : ", "Read All Movie Cast Success");
		hmMovieCast.put("Total : ", castDtos.size());
		hmMovieCast.put("Data : " , castDtos);
		
		return hmMovieCast;
	
	}
	
	//Create Movie Cast
	@PostMapping("/create/movie_cast")
	public HashMap<String, Object> createMovieCast(@Valid @RequestBody MovieCastDto movieCastDtoDetails){
		HashMap<String, Object> hmMovieCast = new HashMap<String, Object>();
		MovieCast cast = new MovieCast(movieCastDtoDetails.getActor(), movieCastDtoDetails.getMovie(), movieCastDtoDetails.getRole());
			
		hmMovieCast.put("Message :", "Create Movie Cast Succes");
		hmMovieCast.put("Data : ", movieCastRepository.save(cast));
		
		return hmMovieCast;
	}
	
	//Delete Movie Cast
	@DeleteMapping("/delete/movie_cast/{id}")
	public HashMap<String, Object> deleteMovieCast(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmMovieCast = new HashMap<String, Object>();
		MovieCast cast = movieCastRepository.findById(id).orElseThrow(null);
		
		movieCastRepository.delete(cast);
		
		hmMovieCast.put("Message : ", "Delete Movie Cast Succes");
		hmMovieCast.put("Data :", cast);
		
		return hmMovieCast;
	}
	
	//Update Movie Cast
	@PutMapping("/update/movie_cast/{id}")
	public HashMap<String, Object> updateMovieCast(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieCastDto movieCastDtoDetails){
		HashMap<String, Object> hmMovieCast = new HashMap<String, Object>();
		MovieCast cast = movieCastRepository.findById(id).orElseThrow(null);
		
		
		if(movieCastDtoDetails.getActor() != null) {
			cast.setActor(movieCastDtoDetails.getActor());
		}
		
		if(movieCastDtoDetails.getMovie() != null) {
			cast.setMovie(movieCastDtoDetails.getMovie());
		}
		
		if(movieCastDtoDetails.getRole() != null) {
			cast.setRole(movieCastDtoDetails.getRole());
		}

		movieCastRepository.save(cast);
			
		hmMovieCast.put("Message : ", "Updated Movie Cast Succes");
		hmMovieCast.put("Data : ", cast);
		
		return hmMovieCast;
		}
}