package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.MovieDto;
import com.example.MovieYuslam.model.Movie;
import com.example.MovieYuslam.repository.MovieRepository;


@RestController
@RequestMapping("/api")
public class MovieController {
	
	@Autowired
	MovieRepository movieRepository;
	
	//Get All Movie
	@GetMapping("/read/movie")
	public HashMap<String, Object> readMovie(){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		List<MovieDto> movieDtos = new ArrayList<MovieDto>();
		for(Movie movie : movieRepository.findAll()) {
			MovieDto movieDto = new MovieDto(movie.getId(), movie.getTitle(), movie.getYear(), movie.getTime(), movie.getLang(), movie.getReleaseDate(), movie.getReleaseCountry());
			movieDtos.add(movieDto);
		}
		
		hmMovie.put("Message : ", "Read Movie Succes");
		hmMovie.put("Total : ", movieDtos.size());
		hmMovie.put("Data : ", movieDtos);
		
		return hmMovie;
		
	}
	
	//Create Movie
	@PostMapping("/create/movie")
	public HashMap<String, Object> createMovie(@Valid @RequestBody MovieDto movieDtoDetails){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = new Movie(movieDtoDetails.getId(), movieDtoDetails.getTitle(), movieDtoDetails.getYear(), movieDtoDetails.getTime(), movieDtoDetails.getLang(), movieDtoDetails.getReleaseDate(), movieDtoDetails.getReleaseCountry());
		
		hmMovie.put("Message : ", "Create Movie Succes");
		hmMovie.put("Data : ", movieRepository.save(movie));
		
		return hmMovie;
	}
	
	//Delete Movie
	@DeleteMapping("/delete/movie/{id}")
	public HashMap<String, Object> deleteMovie(@PathVariable (value = "id") Long id){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id).orElseThrow(null);
		
		movieRepository.delete(movie);
		
		hmMovie.put("Message : ", "Delete Movie Succes");
		hmMovie.put("Data : ", movie);
		
		return hmMovie;
		
	}
	
	//Update Movie
	@PutMapping("/update/movie/{id}")
	public HashMap<String, Object> updateMovie(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieDto movieDtoDetails ){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id).orElseThrow(null);
		
		if(movieDtoDetails.getTitle() != null) {
			movie.setTitle(movieDtoDetails.getTitle());
		}
		
		if(movieDtoDetails.getYear() != 0) {
			movie.setYear(movieDtoDetails.getYear());
		}
		
		if(movieDtoDetails.getTime() != 0) {
			movie.setTime(movieDtoDetails.getTime());
		}
		
		if(movieDtoDetails.getLang() != null ) {
			movie.setLang(movieDtoDetails.getLang());
		}
		
		if(movieDtoDetails.getReleaseDate() != null) {
			movie.setReleaseDate(movieDtoDetails.getReleaseDate());
		}
		
		if(movieDtoDetails.getReleaseCountry() != null ) {
			movie.setReleaseCountry(movieDtoDetails.getReleaseCountry());
		}
		
		movieRepository.save(movie);
		
		hmMovie.put("Message : ", "Updated Movie Succes");
		hmMovie.put("Data", movie);
		
		return hmMovie;
		
		
	}
	
}
