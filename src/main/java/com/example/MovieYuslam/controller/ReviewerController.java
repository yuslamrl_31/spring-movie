package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.ReviewerDto;
import com.example.MovieYuslam.model.Reviewer;
import com.example.MovieYuslam.repository.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	
	@Autowired
	ReviewerRepository reviewerRepository;
	
	//Read Reviewer
	@GetMapping("/read/reviewer")
	public HashMap<String, Object> readReviewer(){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		List<ReviewerDto> reviewerDtos = new ArrayList<ReviewerDto>();
		for(Reviewer reviewer : reviewerRepository.findAll()) {
			ReviewerDto reviewerDto = new ReviewerDto(reviewer.getId(), reviewer.getName());
			reviewerDtos.add(reviewerDto);
		}
		
		hmReviewer.put("Message : ", "Read Reviewer Succes");
		hmReviewer.put("Total : ", reviewerDtos.size());
		hmReviewer.put("Data : ", reviewerDtos);
		
		return hmReviewer;
		
	}
	
	//Create Reviewer
	@PostMapping("/create/reviewer")
	public HashMap<String, Object> createReviewer(@Valid @RequestBody ReviewerDto reviewerDtoDetails){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = new Reviewer(reviewerDtoDetails.getId(), reviewerDtoDetails.getName());
		
		hmReviewer.put("Message : ", "Create Reviewer Succes");
		hmReviewer.put("Data : ", reviewerRepository.save(reviewer));
		
		return hmReviewer;
	}
	
	//Delete Reviewer
	@DeleteMapping("/delete/reviewer/{id}")
	public HashMap<String, Object> deleteReviewer(@PathVariable (value = "id") Long id){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(id).orElseThrow(null);
		
		reviewerRepository.delete(reviewer);
		
		hmReviewer.put("Message : ", "Delete Reviewer Succes");
		hmReviewer.put("Data : ", reviewer);
		
		return hmReviewer;
	}
	
	//Update Reviewer
	@PutMapping("/update/reviewer/{id}")
	public HashMap<String, Object> updateReviewer(@PathVariable (value = "id") Long id, @Valid @RequestBody ReviewerDto reviewerDtoDetails ){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(id).orElseThrow(null);
		
		if(reviewerDtoDetails.getName() != null) {
			reviewer.setName(reviewerDtoDetails.getName());
		}
		
		reviewerRepository.save(reviewer);
		
		hmReviewer.put("Message : ", "Updated Reviewer Succes");
		hmReviewer.put("Data : ", reviewer);
		
		return hmReviewer;
	}
	
}
