package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.MovieDirectionDto;
import com.example.MovieYuslam.model.MovieDirection;
import com.example.MovieYuslam.repository.MovieDirectionRepository;

@RestController
@RequestMapping("/api")
public class MovieDirectionController {

	@Autowired
	MovieDirectionRepository movieDirectionRepository;
	
	//Get All Movie Direction
	@GetMapping("/read/movie_direction")
	public HashMap<String, Object> readMovieDirection(){
		HashMap<String, Object> hmMovieDirection = new HashMap<String, Object>();
		List<MovieDirectionDto> directionDtos = new ArrayList<MovieDirectionDto>();
		for(MovieDirection direction : movieDirectionRepository.findAll()) {
			MovieDirectionDto directionDto = new MovieDirectionDto(direction.getDirector(), direction.getMovie());
			directionDtos.add(directionDto);
		}
			
		hmMovieDirection.put("Message : ", "Read All Movie Direction Success");
		hmMovieDirection.put("Total : ", directionDtos.size());			
		hmMovieDirection.put("Data : " , directionDtos);
			
		return hmMovieDirection;
		
		}
	
	//Create Movie Director
	@PostMapping("/create/movie_direction")
	public HashMap<String, Object> createMovieDirection(@Valid @RequestBody MovieDirectionDto movieDirectionDtoDetails){
		HashMap<String, Object> hmMovieDirection = new HashMap<String, Object>();
		MovieDirection direction = new MovieDirection(movieDirectionDtoDetails.getDirector(), movieDirectionDtoDetails.getMovie());
				
		hmMovieDirection.put("Message :", "Create Movie Director Succes");
		hmMovieDirection.put("Data : ", movieDirectionRepository.save(direction));
			
		return hmMovieDirection;
		
		}
	
	//Delete Movie Cast
	@DeleteMapping("/delete/movie_direction/{id}")
	public HashMap<String, Object> deleteMovieDirection(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmMovieDirection = new HashMap<String, Object>();
		MovieDirection direction = movieDirectionRepository.findById(id).orElseThrow(null);
			
		movieDirectionRepository.delete(direction);
			
		hmMovieDirection.put("Message : ", "Delete Movie Director Succes");
		hmMovieDirection.put("Data :", direction);
			
		return hmMovieDirection;
		}
	
	//Update Movie Direction
	@PutMapping("/update/movie_direction/{id}")
	public HashMap<String, Object> updateMovieDirection(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieDirectionDto movieDirectionDtoDetails){
		HashMap<String, Object> hmMovieDirection = new HashMap<String, Object>();
		MovieDirection direction = movieDirectionRepository.findById(id).orElseThrow(null);
			
			
			if(movieDirectionDtoDetails.getDirector() != null) {
				direction.setDirector(movieDirectionDtoDetails.getDirector());
			}
			
			if(movieDirectionDtoDetails.getMovie() != null) {
				direction.setMovie(movieDirectionDtoDetails.getMovie());
			}

			movieDirectionRepository.save(direction);
				
			hmMovieDirection.put("Message : ", "Updated Movie Direction Succes");
			hmMovieDirection.put("Data : ", direction);
			
			return hmMovieDirection;
			}
}
