package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.RatingDto;
import com.example.MovieYuslam.model.Rating;
import com.example.MovieYuslam.repository.RatingRepository;


@RestController
@RequestMapping("/api")

public class RatingController {

	@Autowired
	RatingRepository ratingRepository;
	
		//Get All Rating
		@GetMapping("/read/rating")
		public HashMap<String, Object> readRating(){
			HashMap<String, Object> hmRating = new HashMap<String, Object>();
			List<RatingDto> ratingDtos = new ArrayList<RatingDto>();
			for(Rating rating : ratingRepository.findAll()) {
				RatingDto ratingDto = new RatingDto(rating.getReviewer(), rating.getMovie(), rating.getRevStars(), rating.getNumORatings());
				ratingDtos.add(ratingDto);
			}
					
			hmRating.put("Message : ", "Read All Rating Success");
			hmRating.put("Total : ", ratingDtos.size());			
			hmRating.put("Data : " , ratingDtos);
					
			return hmRating;
				
			}
		
		//Create Rating
		@PostMapping("/create/rating")
		public HashMap<String, Object> createRating(@Valid @RequestBody RatingDto ratingDtoDetails){
			HashMap<String, Object> hmRating = new HashMap<String, Object>();
			Rating rating = new Rating(ratingDtoDetails.getReviewer(), ratingDtoDetails.getMovie(), ratingDtoDetails.getRevStars(), ratingDtoDetails.getNumORatings());
							
			hmRating.put("Message :", "Create Rating Succes");
			hmRating.put("Data : ", ratingRepository.save(rating));
						
			return hmRating;
					
			}
		
		//Delete Rating
		@DeleteMapping("/delete/rating/{id}")
		public HashMap<String, Object> deleteRating(@PathVariable (value = "id") Long id){
			HashMap<String, Object> hmRating = new HashMap<String, Object>();
			Rating rating = ratingRepository.findById(id).orElseThrow(null);
			
			ratingRepository.delete(rating);
			
			hmRating.put("Message : ", "Delete Rating Succes");
			hmRating.put("Data : ", rating);
			
			return hmRating;
		}
		
		//Update Rating
		@PutMapping("/update/rating/{id}")
		public HashMap<String, Object> updateRating(@PathVariable(value = "id") Long id, @Valid @RequestBody RatingDto ratingDtoDetails){
			HashMap<String, Object> hmRating = new HashMap<String, Object>();
			Rating rating = ratingRepository.findById(id).orElseThrow(null);
						
			if(ratingDtoDetails.getMovie() != null) {
				rating.setMovie(ratingDtoDetails.getMovie());
			}
			
			if(ratingDtoDetails.getReviewer() != null) {
				rating.setReviewer(ratingDtoDetails.getReviewer());
			}
			
			if(ratingDtoDetails.getRevStars() != 0) {
				rating.setRevStars(ratingDtoDetails.getRevStars());
			}
			
			if(ratingDtoDetails.getNumORatings() != 0) {
				rating.setNumORatings(ratingDtoDetails.getNumORatings());
			}
			
			ratingRepository.save(rating);
							
			hmRating.put("Message : ", "Updated Rating Succes");
			hmRating.put("Data : ", rating);
						
			return hmRating;
		}
		
}
