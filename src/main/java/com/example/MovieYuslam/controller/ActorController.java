package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.MovieYuslam.dto.ActorDto;
import com.example.MovieYuslam.model.Actor;
import com.example.MovieYuslam.repository.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	@Autowired
	ActorRepository actorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public Actor convertToEntity (ActorDto actorDto) {
		Actor actor = modelMapper.map(actorDto, Actor.class);
		return actor;
	}
	
	public ActorDto convertToDto (Actor actor) {
		ActorDto actorDto = modelMapper.map(actor, ActorDto.class);
		return actorDto;
	}
	
		//Get All Actor Using model mapper
		@GetMapping("/mapper/read/actor")
		public HashMap<String, Object> readActorMapper(){
			HashMap<String, Object> hmActor = new HashMap<String, Object>();
			List<ActorDto> actorDtos = new ArrayList<ActorDto>();
			for(Actor actor : actorRepository.findAll()) {
				actorDtos.add(convertToDto(actor));
			}
			
			hmActor.put("Message : ", "Read All Actor Success");
			hmActor.put("Total : ", actorDtos.size());
			hmActor.put("Data : " , actorDtos);
			
			return hmActor;
		}
	
		//Create Actor Using model mapper
		@PostMapping("/mapper/create/actor")
		public HashMap<String, Object> createActorMapper(@Valid @RequestBody ActorDto actorDtoDetails){
			HashMap<String, Object> hmActor = new HashMap<String, Object>();
			Actor actor = convertToEntity(actorDtoDetails);
			actorRepository.save(actor);
			
			hmActor.put("Message : ", "Create Actor Succes");
			hmActor.put("Data : ", actor);
			
			return hmActor;
		}
		
		//Update Actor using model mapperOxygen - http://download.eclipse.org/releases/oxygenOxygen - http://download.eclipse.org/releases/oxygen
		@PutMapping("/mapper/update/actor/{id}")
		public HashMap<String, Object> updateActorMapper(@PathVariable(value = "id") Long id,@Valid @RequestBody ActorDto actorDtoDetails){
			HashMap<String, Object> hmActor = new HashMap<String, Object>();
			Actor actor = actorRepository.findById(id).orElseThrow(null);
			
			if(actorDtoDetails.getFirstName() != null) {
				actor.setFirstName(actorDtoDetails.getFirstName());
			}
			if(actorDtoDetails.getLastName() != null ) {
				actor.setLastName(actor.getLastName());
			}
			if(actorDtoDetails.getGender() != null) {
				actor.setGender(actorDtoDetails.getGender());
			}
			
			actor = convertToEntity(actorDtoDetails);
			
			actorRepository.save(actor);
			
			hmActor.put("Message : ", "Updated Actor Succes");
			hmActor.put("Data : ", actor);
			
			return hmActor;
			
		}
		
		
	
		
	
	
	
	
	//Get All Actor
	@GetMapping("/read/actor")
	public HashMap<String, Object> readActor(){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		List<ActorDto> actorDtos = new ArrayList<ActorDto>();
		for(Actor actor : actorRepository.findAll()) {
			ActorDto actorDto = new ActorDto(actor.getId(), actor.getFirstName(), actor.getLastName(), actor.getGender());
			actorDtos.add(actorDto);
		}
		
		hmActor.put("Message : ", "Read All Actor Success");
		hmActor.put("Total : ", actorDtos.size());
		hmActor.put("Data : " , actorDtos);
		
		return hmActor;
	}
	
	//Create Actor
	@PostMapping("/create/actor")
	public HashMap<String, Object> createActor(@Valid @RequestBody ActorDto actorDtoDetails){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = new Actor(actorDtoDetails.getId(),actorDtoDetails.getFirstName(), actorDtoDetails.getGender(), actorDtoDetails.getLastName());
		
		hmActor.put("Message : ", "Create Actor Succes");
		hmActor.put("Data : ", actorRepository.save(actor));
		
		return hmActor;
	}
	
	//Delete Actor
	@DeleteMapping("/delete/actor/{id}")
	public HashMap<String, Object> deleteActor(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(id).orElseThrow(null);
		
		actorRepository.delete(actor);
		
		hmActor.put("Message : ", "Delete Actor Succes");
		hmActor.put("Data : ", actor);
		
		return hmActor;
		
	}
	
	//Update Actor
	@PutMapping("/update/actor/{id}")
	public HashMap<String, Object> updateActor(@PathVariable(value = "id") Long id,@Valid @RequestBody ActorDto actorDtoDetails){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(id).orElseThrow(null);
		
		if(actorDtoDetails.getFirstName() != null) {
			actor.setFirstName(actorDtoDetails.getFirstName());
		}
		if(actorDtoDetails.getLastName() != null ) {
			actor.setLastName(actor.getLastName());
		}
		if(actorDtoDetails.getGender() != null) {
			actor.setGender(actorDtoDetails.getGender());
		}
		
		actorRepository.save(actor);
		
		hmActor.put("Message : ", "Updated Actor Succes");
		hmActor.put("Data : ", actor);
		
		return hmActor;
		
	}
	
	
	
	
}
