package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.GenresDto;
import com.example.MovieYuslam.model.Genres;
import com.example.MovieYuslam.repository.GenresRepository;


@RestController
@RequestMapping("/api")
public class GenresController {

	@Autowired
	GenresRepository genresRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public Genres convertToEntity (GenresDto genresDto) {
		Genres genres = modelMapper.map(genresDto, Genres.class);
		return genres;
	}
	
	public GenresDto convertToDto (Genres genres) {
		GenresDto genresDto = modelMapper.map(genres, GenresDto.class);
		return genresDto;
	}
	
	//Get All Genres Using Model Mapper
	@GetMapping("/mapper/read/genres")
	public HashMap<String, Object> readGenresMapper(){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		List<GenresDto> genresDtos = new ArrayList<GenresDto>();
			for(Genres genres : genresRepository.findAll()) {
				genresDtos.add(convertToDto(genres));
			}
			
			hmGenres.put("Message : ", "Read All Genres Success");
			hmGenres.put("Total : ", genresDtos.size());
			hmGenres.put("Data : " , genresDtos);
			
			return hmGenres;
		}
	
	//Create Genres Using model mapper
	@PostMapping("/mapper/create/genres")
	public HashMap<String, Object> createGenresMapper(@Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = convertToEntity(genresDtoDetails);
				
		hmGenres.put("Message :", "Create Genres Succes");
		hmGenres.put("Data : ", genresRepository.save(genres));
			
		return hmGenres;
	}
	
	//Update Genres Using model mapper
	@PutMapping("/mapper/update/genres/{id}")
	public HashMap<String, Object> updateGenresMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id).orElseThrow(null);
			
			if(genresDtoDetails.getTitle() != null) {
				genres.setTitle(genresDtoDetails.getTitle());
			}
			
			genres = convertToEntity(genresDtoDetails);
			
			genresRepository.save(genres);
			
			hmGenres.put("Message : ", "Updated Director Succes");
			hmGenres.put("Data : ", genres);
			
			return hmGenres;
		}
	
	
	//Get All Genres
	@GetMapping("/read/genres")
	public HashMap<String, Object> readGenres(){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		List<GenresDto> genresDtos = new ArrayList<GenresDto>();
		for(Genres genres : genresRepository.findAll()) {
			GenresDto genresDto = new GenresDto(genres.getId(), genres.getTitle());
			genresDtos.add(genresDto);
		}
		
		hmGenres.put("Message : ", "Read All Genres Success");
		hmGenres.put("Total : ", genresDtos.size());
		hmGenres.put("Data : " , genresDtos);
		
		return hmGenres;
	}
	
	//Create Genres
	@PostMapping("/create/genres")
	public HashMap<String, Object> createGenres(@Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = new Genres(genresDtoDetails.getId(), genresDtoDetails.getTitle());
			
		hmGenres.put("Message :", "Create Genres Succes");
		hmGenres.put("Data : ", genresRepository.save(genres));
		
		return hmGenres;
	}
	
	//Delete Genres
	@DeleteMapping("/delete/genres/{id}")
	public HashMap<String, Object> deleteGenres(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id).orElseThrow(null);
		
		genresRepository.delete(genres);
		
		hmGenres.put("Message : ", "Delete Director Succes");
		hmGenres.put("Data :", genres);
		
		return hmGenres;
	}
	
	//Update Genres
	@PutMapping("/update/genres/{id}")
	public HashMap<String, Object> updateGenres(@PathVariable(value = "id") Long id, @Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id).orElseThrow(null);
		
		if(genresDtoDetails.getTitle() != null) {
			genres.setTitle(genresDtoDetails.getTitle());
		}
		
		genresRepository.save(genres);
		
		hmGenres.put("Message : ", "Updated Director Succes");
		hmGenres.put("Data : ", genres);
		
		return hmGenres;
	}
	
	
	
	}
