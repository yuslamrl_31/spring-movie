package com.example.MovieYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.MovieYuslam.dto.DirectorDto;
import com.example.MovieYuslam.model.Director;
import com.example.MovieYuslam.repository.DirectorRepository;


@RestController
@RequestMapping("/api")
public class DirectorController {
		
	@Autowired
	DirectorRepository directorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public Director convertToEntity (DirectorDto directorDto) {
		Director director = modelMapper.map(directorDto, Director.class);
		return director;
	}
	
	public DirectorDto convertToDto (Director director) {
		DirectorDto directorDto = modelMapper.map(director, DirectorDto.class);
		return directorDto;
	}
	
		//Get All Director Using Model Mapper
		@GetMapping("/mapper/read/director")
		public HashMap<String, Object> readDirectorMapper(){
			HashMap<String, Object> hmDirector = new HashMap<String, Object>();
			List<DirectorDto> directorDtos = new ArrayList<DirectorDto>();
			for(Director director : directorRepository.findAll()) {				
				directorDtos.add(convertToDto(director));
			}
				
			hmDirector.put("Message : ", "Read All Actor Success");
			hmDirector.put("Total : ", directorDtos.size());
			hmDirector.put("Data : " , directorDtos);
				
			return hmDirector;
		
			}
			
		//Create Director Using Model Mapper
		@PostMapping("/mapper/create/director")
		public HashMap<String, Object> createDirectorMapper(@Valid @RequestBody DirectorDto directorDtoDetails){
				HashMap<String, Object> hmActor = new HashMap<String, Object>();
				Director director = convertToEntity(directorDtoDetails);
				directorRepository.save(director);
					
				hmActor.put("Message : ", "Create Actor Succes");
				hmActor.put("Data : ", director);
					
				return hmActor;
			}
		
		//Update Director using model mapper
		@PutMapping("/mapper/update/director/{id}")
		public HashMap<String, Object> updateDirectorMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody DirectorDto directorDtoDetails){
			HashMap<String, Object> hmDirector = new HashMap<String, Object>();
			Director director = directorRepository.findById(id).orElseThrow(null);
			
			if(directorDtoDetails.getFirstName() != null) {
				director.setFirstName(directorDtoDetails.getFirstName());
			}
			
			if(directorDtoDetails.getLastName() != null) {
				director.setLastName(directorDtoDetails.getLastName());
			}
			
			director = convertToEntity(directorDtoDetails);
			
			directorRepository.save(director);
			
			hmDirector.put("Message : ", "Updated Director Succes");
			hmDirector.put("Data : ", director);
			
			return hmDirector;
		}
	
	
	
	//Get All Director
	@GetMapping("/read/director")
	public HashMap<String, Object> readDirector(){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		List<DirectorDto> directorDtos = new ArrayList<DirectorDto>();
		for(Director director : directorRepository.findAll()) {				
			DirectorDto directorDto = new DirectorDto(director.getId(), director.getFirstName(), director.getLastName());
			directorDtos.add(directorDto);
		}
			
		hmDirector.put("Message : ", "Read All Actor Success");
		hmDirector.put("Total : ", directorDtos.size());
		hmDirector.put("Data : " , directorDtos);
			
		return hmDirector;
	
		}
		
	//Create Director
	@PostMapping("/create/director")
	public HashMap<String, Object> createDirector(@Valid @RequestBody DirectorDto directorDtoDetails){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = new Director(directorDtoDetails.getId(), directorDtoDetails.getFirstName(), directorDtoDetails.getLastName());
		
		hmDirector.put("Message : ", "Create Director Succes");
		hmDirector.put("Data : ", directorRepository.save(director));
		
		return hmDirector;
		
	}
	
	//Delete Director
	@DeleteMapping("/delete/director/{id}")
	public HashMap<String, Object> deleteDirector(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = directorRepository.findById(id).orElseThrow(null);
		
		directorRepository.delete(director);
		
		hmDirector.put("Message : ", "Delete Director Succes");
		hmDirector.put("Data :", director);
		
		return hmDirector;
	}
	
	//Update Director
	@PutMapping("/update/director/{id}")
	public HashMap<String, Object> updateDirector(@PathVariable(value = "id") Long id, @Valid @RequestBody DirectorDto directorDtoDetails){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = directorRepository.findById(id).orElseThrow(null);
		
		if(directorDtoDetails.getFirstName() != null) {
			director.setFirstName(directorDtoDetails.getFirstName());
		}
		
		if(directorDtoDetails.getLastName() != null) {
			director.setLastName(directorDtoDetails.getLastName());
		}
		
		directorRepository.save(director);
		
		hmDirector.put("Message : ", "Updated Director Succes");
		hmDirector.put("Data : ", director);
		
		return hmDirector;
	}	
	

}
